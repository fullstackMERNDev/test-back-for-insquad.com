/*
 * all configs for progect.
 */

let db_name = 'test-task';
let db_user = 'root';
let db_pass = '********';
let db_ds = '';
let mongoDbHost = 'localhost:27017/';

const HOME_DIR = '/'; // for local server
// const HOME_DIR = '//'; // for host
const DEFAULT_PORT = 5001;



const url = `mongodb://localhost:27017/${db_name}`;

// const DEV_TEST = true;
const DEV_TEST = false;


//-------------------------------security options
//-------------------------------security options end


module.exports = {
	mongoURI: url,
	HOME_DIR,
	DEFAULT_PORT,

	DEV_TEST,
	
	// security options
};
