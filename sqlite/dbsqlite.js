const sqlite3 = require('sqlite3').verbose();
const dbMod = sqlite3.OPEN_READWRITE;
var path = require('path');
const dbFile = path.join(__dirname, 'db.db');
const db = new sqlite3.Database( dbFile, dbMod, dbCb );

// console.log('dbFile :>> ', dbFile);
// db.close( dbCbClose );




function dbCb( err ) {
    if ( err ) {
        let errTitle = 'dbCb Error:';
        // console.error( errTitle, err );
        errorHandler( errTitle, err );
        return false;
    }
    
    console.log( 'connect successful' );
}


// ---------------------------------------------------------------

async function initDB() {
    console.log( 'async function initDB' );
    // code
    //     {
    //         "id": 2,
    //         "firstName": "Bob",
    //         "lastName": "Davidson",
    //         "age": 21,
    //         "isFree": true,
    //         "createdAt": "2022-03-25",
    //         "updatedAt": "2022-03-27"
    //    }

    db.serialize(async () => {
        await query( `CREATE TABLE IF NOT EXISTS users ( firstName, lastName, age, isFree, createdAt, updatedAt, id INTEGER PRIMARY KEY )`, 'run' );
    });
}

function dbCbClose( err ) {
    // code
    let errTitle = 'dbCb Error:';
    errorHandler( errTitle, err );
}

function errorHandler( title, err ) {
    console.log( 'errorHandler:' );
    console.error( title, err );
}

// ---------------------------------------------------------------

const query = (command, method = 'all', params = []) => {
    return new Promise((resolve, reject) => {
        db[method](command, params, (error, result) => {
            if (error) {
            reject(error);
            } else {
            // console.log('query result :>> ', result);
            resolve(result);
            }
        });
    });
};
// ---------------------------------------------------------------

// ---------------------------------------------------------------

async function insertToDB( valuesArr ) {
    const sql = `INSERT INTO users
                ( firstName, lastName, age, isFree, createdAt, updatedAt )
                VALUES (?, ?, ?, ?, ?, ?);
                `;
    db.run( sql, valuesArr, insCB );
}

function insCB( err ) {
    if ( err ) {
        errorHandler( 'insCB', err );
        return false;
    }

    console.log( 'A new row has been created' );
}

async function selectByWhere( where ) {
    console.log( 'enter to selectAllFromDB' );
    const sql = `SELECT * from users WHERE (${where})`;

    const p = new Promise(  async ( resolve, reject ) => {
        
        db.serialize(async () => {
            const existingPosts = await query( sql );
            resolve( existingPosts );
        });

    }  );
    return p;
}

async function deleteDBByWhere( whereStr ) {
    // code
    console.log( 'enter to deleteDBByWhere' );
    const sql = `DELETE FROM users WHERE (${whereStr})`;
    
    db.serialize(async () => {
        const delPosts = await query( sql );
        return( delPosts );
    });
}
// ---------------------------------------------------------------


module.exports = {
    db,
    initDB,
    dbCbClose,
    query,

    // for jest test
    insertToDB,
    selectByWhere,
    deleteDBByWhere,
};