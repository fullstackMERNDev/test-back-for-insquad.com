const { insertToDB, selectByWhere, deleteDBByWhere } = require("../sqlite/dbsqlite");

test('insert and select user', async () => {
    tmpUser = {
        // "id": 2,
        "firstName": "test",
        "lastName": "test",
        "age": 21,
        "isFree": true,
        "createdAt": "2022-03-25",
        "updatedAt": "2022-03-27"
    };
    let expected = {
        "age": 21,
        "createdAt": "2022-03-25",
        "firstName": "test",
        "isFree": 1,
        "lastName": "test",
        "updatedAt": "2022-03-27",
    };
    async function t1() {
        let sRes = await selectByWhere( `"firstName" = 'test' AND "lastName" = 'test'` );
        let { id, ...tmp } = sRes[0];
        // del test row
        await deleteDBByWhere( `"firstName" = 'test' AND "lastName" = 'test'` );
        return tmp;
    }
    await deleteDBByWhere( `"firstName" = 'test' AND "lastName" = 'test'` );
    await insertToDB( Object.values(tmpUser) );
    expect( await t1() ).toStrictEqual(expected);
})
