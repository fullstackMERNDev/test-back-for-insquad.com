/* 
 *  "\routes\users.js"
 */

// подключение библиотек и т.п.
const express = require('express');

// подключение своих файлов
const controller = require('../controllers/users');

const router = express.Router();


// ---------------------------------------------------------------------------------
router.get( '/', 
    controller.getAll 
);
    
router.get( '/:id',
    controller.getById 
);
    
router.delete( '/:id',
    controller.remove 
);
    
router.post( '/',
    controller.create 
);

// router.put( '/:id',
router.patch( '/:id',
    controller.update 
);


module.exports = router;