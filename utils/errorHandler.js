/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


module.exports = ( res, error, status = 500 ) => {
    res.status( status ).json({
        status,
        success : false,
        message : error.message ? error.message : error
    });
};