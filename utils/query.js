const { db } = require("../sqlite/dbsqlite");

const query = (command, method = 'all', params = [] ) => {
    return new Promise((resolve, reject) => {
        db.serialize(function() {
            db[method](command, params, (error, result) => {
                if (error) {
                    console.log('command :>> ', command);
                    console.log('params :>> ', params);
                    reject(error);
                } else {
                    console.log('command :>> ', command);
                    console.log('params :>> ', params);
                    console.log('result :>> ', result);
                    resolve(result);
                }
            });
        });
    });
};

module.exports = query;