const express = require( 'express' );
const app = express();
const http = require( 'http' );
const server = http.createServer( app );

// подключение body-parser
const bodyParser = require ( 'body-parser' );


const keys = require ( './config/keys' );
require('./sqlite/dbsqlite');
const { initDB, db, dbCbClose, insertToDB, selectByWhere } = require('./sqlite/dbsqlite');

// подключение роутов
const usersRoutes = require('./routes/users');
const query = require('./utils/query');


// для парсинга запроса на сервер
app.use(  bodyParser.urlencoded( { extended : true } )  );
app.use( bodyParser.json() );
// app.use(  express.static( 'public' )  );

// назначение контроллеров-обработчиков на урл
app.use( '/api/users', usersRoutes );


const port = keys.DEFAULT_PORT || 5000;

// ---------------------------------------------------------------

// ---------------------------------------------------------------
// start ONLY ONE TIME for CREATE DB
initDB();

// db.close( dbCbClose );
// // ---------------------------------------------------------------



server.listen( port, () => {
    console.log( `listening on *:${port}`);
});






