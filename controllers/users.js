/* 
 * "\controllers\users.js"
 */


// подключение своих ресурсов -- контроллеры, модели и пр.
// подключить модель.
// const Users = require('../models/Users'); // TODO (?) -- в текущем проекте, не уверен, что нужно повышать уровень до выделения отдельной модели


// обработчик ошибок -- универсальный самописный...
const { db, dbCbClose } = require('../sqlite/dbsqlite');
const errorHandler = require('../utils/errorHandler');
const query = require('../utils/query');



/**
 * action получения всех юзеров.
 */
module.exports.getAll = async function ( req, res ) {
    try {
        const sql = 'SELECT * from users';
        const users = await query( sql );
        
        res.status(200).json( users );
        
    } catch (e) {
        // ОБРАБОТАТЬ ошибку (!)
        errorHandler( res, e );        
    }
};

/**
 * action получения юзера по ИД.
 */
module.exports.getById = async function ( req, res ) {
    try {
        // const users = await Users.findById( req.params.id );
        const sql = 'SELECT * from users WHERE id=' + req.params.id;
        const users = await query( sql, 'get' );
        
        res.status(200).json( users );

    } catch (e) {
        // ОБРАБОТАТЬ ошибку (!)
        errorHandler( res, e );        
    }
};

/**
 * action удаления юзера по ИД.
 */
module.exports.remove = async function ( req, res ) {
    console.log('remove :>> ', 'remove');
    try {
        // await Users.remove({
            //     _id: req.params.id
            // });
            const id = req.params.id;
            let sql = `DELETE FROM users WHERE id=(?)`;
            const result = await query( sql, 'run', id );
            
        res.status(200).json({
            message : `User with id: ${id} was deleted`
        });
    } catch (e) {
        // ОБРАБОТАТЬ ошибку (!)
        errorHandler( res, e );        
    }
};

/**
 * action создания юзера.
 */
module.exports.create = async function ( req, res ) {
    console.log( 'create' );
    // подготовка данных из формы
    // const users = new Users({
    //     name : req.body.name,
    //     user : req.user.id
    // });
    const {
        firstName,
        lastName,
        age,
        isFree,
        createdAt,
        updatedAt
    } = req.body;
    const params = [ firstName, lastName, age, isFree, createdAt, updatedAt ];
    try {
        // await users.save();
        const sql = `INSERT INTO users
        ( firstName, lastName, age, isFree, createdAt, updatedAt )
        VALUES (?, ?, ?, ?, ?, ?);
        `;
        
        let users = await query( sql, 'run', params);
        console.log('users :>> ', users);
        
        res.status(201).json( { message: `user was creating` } );

    } catch (e) {
        // ОБРАБОТАТЬ ошибку (!)
        errorHandler( res, e );        
    }
};

/**
 * action изменения юзера.
 */
module.exports.update = async function ( req, res ) {
    console.log('action изменения юзера', req, res);
    
    // const updated = {
    //     name : req.body.name,
    // };
    
    try {
        // const users = await Users.findOneAndUpdate(
        //     { _id : req.params.id },
        //     { $set : updated },
        //     { new : true }
        // );
        const id = req.params.id;
        // допустимые пар-ры
        const {
            firstName,
            lastName,
            age,
            isFree,
            createdAt,
            updatedAt
        } = req.body;
        const params = { firstName, lastName, age, isFree, createdAt, updatedAt };
        let tmpParams = '';

        for (const [key, val] of Object.entries(params) ) {
            if ( val === undefined ) {
                continue;
            }
            console.log('key :>> ', key);
            console.log('val :>> ', val);
            tmpParams += `${key}='${val}', `;
        }
        tmpParams = tmpParams.trim().slice( 0, -1 );
        
        const sql = `UPDATE main.users
        SET ${tmpParams}
        WHERE
            "id"='${id}';`;
        let result = await query( sql, 'run' );

        res.status(200).json( { message: 'row updated' } );

    } catch (e) {
        // ОБРАБОТАТЬ ошибку (!)
        errorHandler( res, e );        
    }
};


